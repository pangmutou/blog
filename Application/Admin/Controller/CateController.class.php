<?php
namespace Admin\Controller;
use Think\Controller;
class CateController extends Controller {
    /**
     * 查询分页
     */
    public function page(){
        $cate = D("cate");//实例化
        $cateList = $cate->select();
        $this->assign("cateList",$cateList);
        $this->display();
    }

    /**
     * 跳往添加界面
     */
    public function toAdd(){
        $this->display("add");
    }

    /**
     * 执行添加的操作
     */
    public function doAdd(){
        $cate = D("cate");
        if(IS_POST){
            $date["cate_name"] = I("cate_name");
            if($cate->create($date)){
                if($cate->add($date)){
                    $this->success('新增成功', '/admin/cate/page');
                }else{
                    $this->error("新增失败");
                }
            }else{
                $this->error($cate->getError());
            }

        }

    }

    /**
     * 跳往修改的界面
     */
    public function toUpdate(){
        //需要带值到页面
        $cate = D("cate");
        $CateEntity = $cate->where("cate_id=".I("cate_id"))->find();
        $this->assign("cateEntity",$CateEntity);
        $this->display("update");
    }

    /**
     * 执行修改的操作
     */
    public function doUpdate(){
        $this->success('修改成功', '/admin/cate/page');
    }

    /**
     * 执行删除操作
     */
    public function doDelete(){
        $cate = D("cate");
        if($cate->delete(I("cate_id"))){
            $this->success('删除成功', '/admin/cate/page');
        }else{
            $this->error();
        }
    }
}