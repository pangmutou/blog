<?php
namespace Admin\Model;
use Think\Model;

class CateModel extends Model {

    protected $_validate = array(
        array('cate_name','require','栏目名称不能为空！',1,'regex',3), // 在新增的时候验证name字段是否唯一
        array('cate_name','','栏目名称不能为空！',1,'unique',3), // 在新增的时候验证name字段是否唯一
    );

}